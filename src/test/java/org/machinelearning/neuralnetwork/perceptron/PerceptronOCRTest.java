package org.machinelearning.neuralnetwork.perceptron;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.machinelearning.neuralnetwork.perceptron.core.PerceptronOCR;

/**
 * Unit test for simple App.
 */
public class PerceptronOCRTest
{
	private PerceptronOCR perceptronOCR;
	
	/*
		. . # # . . .
		. . . # . . .
		. . . # . . .
		. . # . # . .
		. . # . # . .
		. # # # # # .
		. # . . . # .
		. # . . . # .
		# # # . # # #
		A . . . . . .
	*/
	
	private final Integer[] inputA = {
								0, 0, 1, 1, 0, 0, 0,
								0, 0, 0, 1, 0, 0, 0,
								0, 0, 0, 1, 0, 0, 0,
								0, 0, 1, 0, 1, 0, 0,
								0, 0, 1, 0, 1, 0, 0,
								0, 1, 1, 1, 1, 1, 0,
								0, 1, 0, 0, 0, 1, 0,
								0, 1, 0, 0, 0, 1, 0,
								1, 1, 1, 0, 1, 1, 1
									}; 
	
	private final Integer[] expectedOutputA = {1, -1, -1, -1, -1, -1, -1};
	
	
	/*
		# # # # # # .
		. # . . . . #
		. # . . . . #
		. # . . . . #
		. # # # # # .
		. # . . . . #
		. # . . . . #
		. # . . . . #
		# # # # # # .
		. B . . . . .
	 */

	private final Integer[] inputB = {
									1, 1, 1, 1, 1, 1, 0,
									0, 1, 0, 0, 0, 0, 1,
									0, 1, 0, 0, 0, 0, 1,
									0, 1, 0, 0, 0, 0, 1,
									0, 1, 1, 1, 1, 1, 0,
									0, 1, 0, 0, 0, 0, 1,
									0, 1, 0, 0, 0, 0, 1,
									0, 1, 0, 0, 0, 0, 1,
									1, 1, 1, 1, 1, 1, 0
									}; 
	
	private final Integer[] expectedOutputB = {-1, 1, -1, -1, -1, -1, -1};
	
	@Before
    public void setUp() throws Exception {
    	this.initializePerceptron();
    }
    
    private void initializePerceptron(){
    	int numberOfInput = 63;
    	int numberOfNeurons = 7;
    	double learningRate = 0.3;
    	this.perceptronOCR = new PerceptronOCR(numberOfInput, numberOfNeurons, learningRate);
    }

	/**
     * 
     */
    @Test
    public void testLearnA()
    {	
    	    	try {
    		this.perceptronOCR.learn(inputA, expectedOutputA);
    		assertTrue( true );
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue( false );
		}    	
    }
    
    @Test
    public void testLearnB()
    {	
    	    	try {
    		this.perceptronOCR.learn(inputB, expectedOutputB);
    		assertTrue( true );
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue( false );
		}    	
    }
    
    @Test
    public void testGetClassifiedObjectA(){
    	
    	this.initializePerceptron();
    	this.perceptronOCR.learn(inputA, expectedOutputA);
    	
    	Integer[] output = this.perceptronOCR.getClassifiedObject(inputA);
    	
    	System.out.println("A");
    	System.out.println("Expected:   " + Arrays.toString(expectedOutputA));
    	System.out.println("Classified: " + Arrays.toString(output));
    	assertEquals(Arrays.toString(expectedOutputA), Arrays.toString(output));
    }
    
    @Test
    public void testGetClassifiedObjectB(){
    	
    	this.initializePerceptron();
    	this.perceptronOCR.learn(inputB, expectedOutputB);
    	
    	Integer[] output = this.perceptronOCR.getClassifiedObject(inputB);
    	
    	System.out.println("B");
    	System.out.println("Expected:   " + Arrays.toString(expectedOutputB));
    	System.out.println("Classified: " + Arrays.toString(output));
    	assertEquals(Arrays.toString(expectedOutputB), Arrays.toString(output));
    }
    
    @Test
    public void testGetClassifiedObjectAB(){
    	
    	this.initializePerceptron();
    	this.perceptronOCR.learn(inputA, expectedOutputA);
    	this.perceptronOCR.learn(inputB, expectedOutputB);
    	
    	Integer[] outputA = this.perceptronOCR.getClassifiedObject(inputA);
    	
    	Integer[] outputB = this.perceptronOCR.getClassifiedObject(inputB);
    	
    	System.out.println("\nA & B");
    	System.out.println("A");
    	System.out.println("Expected:   " + Arrays.toString(expectedOutputA));
    	System.out.println("Classified: " + Arrays.toString(outputA));
    	assertEquals(Arrays.toString(expectedOutputA), Arrays.toString(outputA));
    	
    	System.out.println("B");
    	System.out.println("Expected:   " + Arrays.toString(expectedOutputB));
    	System.out.println("Classified: " + Arrays.toString(outputB));
    	assertEquals(Arrays.toString(expectedOutputB), Arrays.toString(outputB));
    }
}
