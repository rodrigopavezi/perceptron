package org.machinelearning.neuralnetwork.perceptron;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Map;

import org.junit.Test;
import org.machinelearning.neuralnetwork.perceptron.io.FileParser;


public class FileParserTest {
	
	@Test
	public void testLoadTrainingFile() throws FileNotFoundException{
		File trainingFile = new File(FileParserTest.class.getResource("/train.txt").getFile());
		
		String expected = "train";
		Map<String, Map<Integer[],Integer[]>> inputData = FileParser.loadFile(trainingFile);
		
		for (Map.Entry<String, Map<Integer[],Integer[]>> entry : inputData.entrySet()) {
		    String key = entry.getKey();
		    System.out.println("Type: " + key);
		    Map<Integer[],Integer[]> value = entry.getValue();
		    
		    for (Map.Entry<Integer[],Integer[]> entry2 : value.entrySet()) {
		    	 System.out.println("Input: \n" + Arrays.toString(entry2.getKey()));
		    	 System.out.println("Expected: \n" + Arrays.toString(entry2.getValue()) + "\n");
		    }
		}
		
		assertTrue(inputData.containsKey(expected));
	}
	
	@Test
	public void testLoadTestingFile() throws FileNotFoundException{
		File testingFile = new File(FileParserTest.class.getResource("/test.txt").getFile());
		
		String expected = "test";
		Map<String, Map<Integer[],Integer[]>> inputData = FileParser.loadFile(testingFile);
		
		for (Map.Entry<String, Map<Integer[],Integer[]>> entry : inputData.entrySet()) {
		    String key = entry.getKey();
		    System.out.println("Type: " + key);
		    Map<Integer[],Integer[]> value = entry.getValue();
		    
		    for (Map.Entry<Integer[],Integer[]> entry2 : value.entrySet()) {
		    	 System.out.println("Input: \n" + Arrays.toString(entry2.getKey()));
		    	 System.out.println("Expected: \n" + Arrays.toString(entry2.getValue()) + "\n");
		    }
		}
		
		assertTrue(inputData.containsKey(expected));
	}

}
