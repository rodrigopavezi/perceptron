package org.machinelearning.neuralnetwork.perceptron;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;


import org.junit.Test;
import org.machinelearning.neuralnetwork.perceptron.io.PatternParser;

public class PatternParserTest {
	
	private final String textA = "{\n" +
								  ". . # # . . .\n" +
								  ". . . # . . .\n" +
								  ". . . # . . .\n" +
								  ". . # . # . .\n" +
								  ". . # . # . .\n" +
								  ". # # # # # .\n" +
								  ". # . . . # .\n" +
								  ". # . . . # .\n" +
								  "# # # . # # #\n" +
								  "A . . . . . .\n" +
								  "}\n\n";

	private final String textB = "{\n" +
								  "# # # # # # .\n" +
								  ". # . . . . #\n" +
								  ". # . . . . #\n" +
								  ". # . . . . #\n" +
								  ". # # # # # .\n" +
								  ". # . . . . #\n" +
								  ". # . . . . #\n" +
								  ". # . . . . #\n" +
								  "# # # # # # .\n" +
								  ". B . . . . .\n" +
								  "}\n\n";
	
	
		/*
		. . # # . . .
		. . . # . . .
		. . . # . . .
		. . # . # . .
		. . # . # . .
		. # # # # # .
		. # . . . # .
		. # . . . # .
		# # # . # # #
		A . . . . . .
	*/
	
	private final Integer[] inputA = {
								0, 0, 1, 1, 0, 0, 0,
								0, 0, 0, 1, 0, 0, 0,
								0, 0, 0, 1, 0, 0, 0,
								0, 0, 1, 0, 1, 0, 0,
								0, 0, 1, 0, 1, 0, 0,
								0, 1, 1, 1, 1, 1, 0,
								0, 1, 0, 0, 0, 1, 0,
								0, 1, 0, 0, 0, 1, 0,
								1, 1, 1, 0, 1, 1, 1
									}; 

	private final Integer[] expectedOutputA = {1, 0, 0, 0, 0, 0, 0};
	private final Integer[] expectedBipolarOutputA = {1, -1, -1, -1, -1, -1, -1};
	
	
		/*
		# # # # # # .
		. # . . . . #
		. # . . . . #
		. # . . . . #
		. # # # # # .
		. # . . . . #
		. # . . . . #
		. # . . . . #
		# # # # # # .
		. B . . . . .
	 */
	
	private final Integer[] inputB = {
									1, 1, 1, 1, 1, 1, 0,
									0, 1, 0, 0, 0, 0, 1,
									0, 1, 0, 0, 0, 0, 1,
									0, 1, 0, 0, 0, 0, 1,
									0, 1, 1, 1, 1, 1, 0,
									0, 1, 0, 0, 0, 0, 1,
									0, 1, 0, 0, 0, 0, 1,
									0, 1, 0, 0, 0, 0, 1,
									1, 1, 1, 1, 1, 1, 0
									}; 
	
	private final Integer[] expectedOutputB = {0, 1, 0, 0, 0, 0, 0};
	
	private final Integer[] expectedBipolarOutputB = {-1, 1, -1, -1, -1, -1, -1};

	@Test
	public void testParseTextDataA(){
		
		Map<Integer[], Integer[]> data = PatternParser.parseTextData(textA);
		
		for (Map.Entry<Integer[],Integer[]> entry : data.entrySet()) {
			assertArrayEquals(inputA, entry.getKey());
			assertArrayEquals(expectedBipolarOutputA, entry.getValue());
	    }
		
	}
	
	@Test
	public void testParseTextDataB(){
		
		Map<Integer[], Integer[]> data = PatternParser.parseTextData(textB);
		
		for (Map.Entry<Integer[],Integer[]> entry : data.entrySet()) {
			assertArrayEquals(inputB, entry.getKey());
			assertArrayEquals(expectedBipolarOutputB, entry.getValue());
	    }
		
	}
	
	@Test
	public void testParseNumericDataA(){
			
		Map<Integer[],Integer[]> data = new HashMap<Integer[], Integer[]>();
		
		data.put(inputA, expectedOutputA);
			
		String textData = PatternParser.parseNumericData(data);
		
		String expected = textA;
		assertEquals(expected, textData);
		
	}
	
	@Test
	public void testParseNumericDataAB(){
			
		Map<Integer[],Integer[]> data = new HashMap<Integer[], Integer[]>();
		
		data.put(inputA, expectedOutputA);
		data.put(inputB, expectedOutputB);
			
		String textData = PatternParser.parseNumericData(data);
		
		assertTrue(textData.contains(textA) && textData.contains(textB));
		
	}
	
	@Test
	public void testGetValuesWithCharacterA(){
		String values = "[1, 0, 0, 0, 0, 0, 0]";
		String expected = "[A, 0, 0, 0, 0, 0, 0]";
		
		String valuesWithCharacters = PatternParser.getValuesWithCharacter(values);
		
		assertEquals(expected, valuesWithCharacters);
	}
	
	@Test
	public void testGetValuesWithCharacterB(){
		String values = "[0, 1, 0, 0, 0, 0, 0]";
		String expected = "[0, B, 0, 0, 0, 0, 0]";
		
		String valuesWithCharacters = PatternParser.getValuesWithCharacter(values);
		
		assertEquals(expected, valuesWithCharacters);
	}
	
	@Test
	public void testGetValuesWithCharacterBK(){
		String values = "[0, 1, 0, 0, 0, 0, 1]";
		String expected = "[0, B, 0, 0, 0, 0, K]";
		
		String valuesWithCharacters = PatternParser.getValuesWithCharacter(values);
		
		assertEquals(expected, valuesWithCharacters);
	}
	
	@Test
	public void testGetValuesWithCharacterBKWithoutBrakets(){
		String values = "0 1 0 0 0 0 1";
		String expected = "0 B 0 0 0 0 K";
		
		String valuesWithCharacters = PatternParser.getValuesWithCharacter(values);
		
		assertEquals(expected, valuesWithCharacters);
	}

}
