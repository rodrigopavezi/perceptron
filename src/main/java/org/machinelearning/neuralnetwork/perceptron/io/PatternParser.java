package org.machinelearning.neuralnetwork.perceptron.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternParser {
	
	public static Map<Integer[], Integer[]> parseTextData(String text){
		Map<Integer[], Integer[]> data = new HashMap<Integer[], Integer[]>();
		
		Pattern patternInput = Pattern.compile("\\{\n(\\W*\n+)(.*\n*)\\}");
			
		Matcher matcher = patternInput.matcher(text);
		
		while(matcher.find()){
			String input = matcher.group(1);
			String expected = matcher.group(2);
			
			//System.out.println(input);
			//System.out.println(expected);
			
			input = input.replaceAll("\\.", "0");
			input = input.replaceAll("#", "1");
			
			expected = expected.replaceAll("\\w", "1");
			expected = expected.replaceAll("\\.", "-1");
			
			String[] inputValues = input.split("\\s");
			String[] expectedValues = expected.split("\\s");
			
			//System.out.println(Arrays.toString(inputValues));
			//System.out.println(Arrays.toString(expectedValues));
			
			data.put(convertToInteger(inputValues), convertToInteger(expectedValues));
		}
		
		return data;
		
	}
	
	public static String parseNumericData(Map<Integer[], Integer[]> data){
		 String textData = "";
		 
		 for (Map.Entry<Integer[],Integer[]> entry : data.entrySet()) {
			 
			
			 String inputAsString = Arrays.toString(entry.getKey());
			 inputAsString = inputAsString.substring(1, inputAsString.length()-1);
			 inputAsString = inputAsString.replaceAll(" ", "");
			 
			 ArrayList<String> inputValues = new ArrayList<String>(Arrays.asList(inputAsString.split(",")));
			 int index = 7;
			 while( index <= inputValues.size()){
				 inputValues.add(index, "\n");
				 index += 8;
			 }
			 String input = inputValues.toString();
			 input = input.replaceAll(" ", "");
			 input = input.replaceAll(",\n,*", "\n");
			 input = input.substring(1, input.length()-1).replaceAll(",", " ");
			 input = input.replaceAll("0", ".");
			 input = input.replaceAll("1", "#");
			 
			 
			 String expected = Arrays.toString(entry.getValue());
			 expected = expected.substring(1, expected.length()-1).replaceAll(",", "");
			 expected = expected.replaceAll("0", ".");
			 
			 expected = getValuesWithCharacter(expected);
			 expected += "\n";
			 
			 textData += "{\n" + input + expected + "}\n\n";
			 
		 }
		
		return textData;
		
	}
	
	public static String getValuesWithCharacter(String values){
		String valuesWithCharacter =  values;
		String[] characters = {"A","B","C","D","E","J","K"};
		 
		while(valuesWithCharacter.contains("1")){
			String valuesWithoutSpace = valuesWithCharacter.replaceAll(" ", "");
			if(valuesWithoutSpace.contains("[")){
				valuesWithoutSpace = valuesWithoutSpace.substring(1, valuesWithoutSpace.length()-1).replaceAll(",", "");
			}
			int position = valuesWithoutSpace.indexOf("1");
			valuesWithCharacter = valuesWithCharacter.replaceFirst("1", characters[position]);
		}
		
		return valuesWithCharacter;
		
	}
	
	private static Integer[] convertToInteger(String[] values){
		Integer[] integerValues = new Integer[values.length];
		
		for (int i = 0; i < values.length; i++) {
			integerValues[i] = Integer.parseInt(values[i]);
		}
		
		return integerValues;
	}
	
}
