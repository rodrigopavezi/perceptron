package org.machinelearning.neuralnetwork.perceptron.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

public class FileParser {
	
	
	public static Map<String,Map<Integer[],Integer[]>> loadFile(File inputFile) throws FileNotFoundException{
		
		Map<String,Map<Integer[],Integer[]>> inputData = new HashMap<String, Map<Integer[],Integer[]>>();
		
		processLineByLine(inputFile, inputData);				
		
		return inputData;
	}
	
	private static void processLineByLine(File inputFile, Map<String,Map<Integer[],Integer[]>> inputData) throws FileNotFoundException {

		Scanner scanner = new Scanner(new FileReader(inputFile));
	    try {
	    	Map<Integer[],Integer[]> inputAndExpectedMap = new HashMap<Integer[], Integer[]>();
	    	
	    	while ( scanner.hasNextLine() ){
		    	String line = scanner.nextLine();
		    	Scanner scannerLine = new Scanner(line);
				String found = scannerLine.findInLine(Pattern.compile("<(\\w*)>"));
				
				
				if(found != null){
					MatchResult result = scannerLine.match();
					String inputType = result.group(1);
					inputData.put(inputType, inputAndExpectedMap);
				}else{
					processLine( scannerLine, inputAndExpectedMap);
				}
	    	}
	    }
	    finally {
	      scanner.close();
	    }
	  }

	private static void processLine(Scanner scannerLine,
				Map<Integer[], Integer[]> inputAndExpectedMap) {
		scannerLine.useDelimiter("=");
	    if ( scannerLine.hasNext() ){
	      String[] inputValues = scannerLine.next().trim().split("\\s");
	      String[] expectedValues = scannerLine.next().trim().split("\\s");
	      
	      inputAndExpectedMap.put(convertToInteger(inputValues), convertToInteger(expectedValues));
	      
	    }
	 }		
	

	private static Integer[] convertToInteger(String[] values){
		Integer[] integerValues = new Integer[values.length];
		
		for (int i = 0; i < values.length; i++) {
			integerValues[i] = Integer.parseInt(values[i]);
		}
		
		return integerValues;
	}
}
