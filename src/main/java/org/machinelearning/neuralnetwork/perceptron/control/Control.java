package org.machinelearning.neuralnetwork.perceptron.control;

import java.util.HashMap;
import java.util.Map;

import org.machinelearning.neuralnetwork.perceptron.core.PerceptronOCR;

public class Control {
	private final int NUMBER_OF_INPUT = 63;
	private final int NUMBER_OF_NEURONS = 7;
	private PerceptronOCR perceptronOCR;
		
	public Control(){
		
	}
	
	public void train(Map<Integer[],Integer[]> data, double learningRate, int iterations){
		System.out.println("Train");
    	perceptronOCR = new PerceptronOCR(NUMBER_OF_INPUT, NUMBER_OF_NEURONS, learningRate);
    	
    	for (int i = 0; i < iterations; i++) {
		
	    	for (Map.Entry<Integer[],Integer[]> entry : data.entrySet()) {
	    		Integer[] input = entry.getKey();
	    		Integer[] expected = entry.getValue();
	    		perceptronOCR.learn(input, expected);
			}
    	}
	}
	
	public Map<Integer[],Integer[]> classify(Map<Integer[],Integer[]> data){
		Map<Integer[],Integer[]> results = new HashMap<Integer[], Integer[]>();
		
		for (Map.Entry<Integer[],Integer[]> entry : data.entrySet()) {
    		Integer[] input = entry.getKey();
    		Integer[] expected = entry.getValue();
    		
    		Integer[] output = perceptronOCR.getClassifiedObject(input);
    		
    		results.put(expected, output);
		}
		
		return results;
	}
}
