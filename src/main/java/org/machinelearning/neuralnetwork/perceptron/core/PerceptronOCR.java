package org.machinelearning.neuralnetwork.perceptron.core;



public class PerceptronOCR implements Perceptron
{
	private Neuron[] neurons;
	
	public PerceptronOCR(int numberOfInput, int numberOfNeurons, double learningRate){
		this.neurons = new Neuron[numberOfNeurons];
		
		for (int i = 0; i < neurons.length; i++) {
			neurons[i] = new NeuronImpl(numberOfInput, learningRate);
		}
	}
	
	public Neuron[] getNeurons() {
		return neurons;
	}

	public void setNeurons(Neuron[] neurons) {
		this.neurons = neurons;
	}

	public void learn(Integer[] input, Integer[] expectedOutput) {
		boolean learned = false;
		
		while (!learned){
			learned = true;
			
			for (int i = 0; i < this.neurons.length; i++) {
				Neuron neuron = this.neurons[i];
				int expectedValue = expectedOutput[i];
				int output = neuron.process(input);
				
				if(output != expectedValue){
					learned = false;
					neuron.updateFactors(expectedValue, input);
				}
			}
		}
	}

	public Integer[] getClassifiedObject(Integer[] input) {
		Integer[] output = new Integer[this.neurons.length];
		
		for (int i = 0; i < this.neurons.length; i++) {
			Neuron neuron = this.neurons[i];
			output[i] = neuron.process(input);
		}
		
		return output;
	}

}
