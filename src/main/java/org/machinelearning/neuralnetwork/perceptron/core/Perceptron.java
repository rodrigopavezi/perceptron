package org.machinelearning.neuralnetwork.perceptron.core;

/**
 * @author rsp
 *
 */
public interface Perceptron {
	
	public void learn(Integer[] input, Integer[] expectedValue);
		
	public Integer[] getClassifiedObject(Integer[] input);
}
