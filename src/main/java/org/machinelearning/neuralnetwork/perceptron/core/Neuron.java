package org.machinelearning.neuralnetwork.perceptron.core;

public interface Neuron {
	
	public int process(Integer[] input);
	
	public void updateFactors(int expectedValue, Integer[] input);
	
}
