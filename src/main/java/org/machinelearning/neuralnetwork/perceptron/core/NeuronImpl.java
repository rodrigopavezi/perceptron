package org.machinelearning.neuralnetwork.perceptron.core;

import java.util.Arrays;

public class NeuronImpl implements Neuron {
	
	private double bias;
	private double[] weights;
	private double learingRate;
	
	public NeuronImpl(int numberOfInput, double learningRate){
		
		this.weights = new double[numberOfInput];
		
		for (int i = 0; i < numberOfInput; i++) {
			//Initialize weights with random values from -1 to 1.
			this.weights[i] = Math.random() * 2 - 1;
			//Initialize weights with random values from 0 to 0.5.
			//this.weights[i] = Math.random() * 0.5;
		}
		
		System.out.println("Weights: " +Arrays.toString(this.weights));
		
		//Initialize bias with a random vale from -1 to 1
		this.bias = Math.random() * 2 -1;
		//Initialize bias with a random vale from 0 to 0.5
		//this.bias = Math.random() * 0.5;
		System.out.println("Bias: " + this.bias);
		
		this.learingRate = learningRate;
		
	}
	
	public double getBias() {
		return bias;
	}


	public void setBias(double bias) {
		this.bias = bias;
	}
	
	
	public int process(Integer[] input) {
		double innerOutput = 0;
		double weightedSum = 0;
		for (int i = 0; i < input.length; i++) {
			weightedSum += input[i] * this.weights[i];
		}
		
		innerOutput = this.bias + weightedSum;
		int output = activate(innerOutput);
		
		return output;
	}
	
	private int activate(double innerOutput){
		
		int activation = 0;
		if(innerOutput >= 0){
			activation = 1;
		}else{
			activation = -1;
		}
			
		return activation;
	}


	public void updateFactors(int expectedValue, Integer[] input) {
			updateWeight(expectedValue,input);
			updateBias(expectedValue);
	}
	
	private void updateWeight(int expectedValue, Integer[] input){
		for (int i = 0; i < this.weights.length; i++) {
			this.weights[i] = this.weights[i] + learingRate * expectedValue * input[i]; 
		}
	}
	
	private void updateBias(int expectedValue){
		this.bias = this.bias + this.learingRate * expectedValue;
	}

}
