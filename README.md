Perceptron implementation for OCR
=================================

Please have a look at the [Demo](https://googledrive.com/host/0B2lmyvF8ChM1NWtaMVFsSG9idGM/perceptron.html "https://googledrive.com/host/0B2lmyvF8ChM1NWtaMVFsSG9idGM/perceptron.html")

Implementations
--------------

A perceptron for OCR was implemented using Java applet technology with the following
features:

1. Load of training and testing data files.
2. Training and testing data manipulation.
3. Input of learning rate.
4. Input of number of iteration.
5. Output of result.
6. Clear results.


Given training and testing data files were modified for a easier parsing purpose. Now they
should contain a tag on the top of the file. e.g. <train\> and <test\> and a equal sign “=” for
separating input data from expected output.

Perceptron initialization
-------------------------

The algorithm initialize the neurons with random values between -1 and 1 assigned to each of
the related input value weight and to the bias variable.

Tests
-----

A couple of tests were performed to analyze the generalization performance of the implemented
perceptron for the given problem. Some tests run with different learning rates and others with
different initialization which took different random ranges for values to the bias and weights. It
was also applied different iterations values throughout tests.

Results
-------

Tests results showed a good generalization performance for classifying the validation set when
applying specific values the learning rating and iterations parameters. For instance, setting the
learning rate to a high value of 0.9 and the iterations to a value over 10.

After more tests it was possible to notice that the number of iterations during the training are
very important for building a classifier with a good generalization performance. This happened
when training with only 1 iteration, thus making it harder to generalize.

On the other hand, when applying a big number of iterations, i.e. 1000, to the training, it would
decreased the generalization, consequently getting a pour classification for the validation set.

To conclude, it is understandable that to build a good classifier with a good generalization
performance, it is needed to look after the number of iteration over the training set so the neural
network will be able to capture the nuances of the different patterns as well as not overfit.

For future work, a validation step during training could be implemented. This would validate and
capture the error outputs after each iteration then use it to stop the training when error starts to
increase.


